import { Component, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-bubble-image',
  templateUrl: './bubble-image.component.html',
  styleUrls: ['./bubble-image.component.scss']
})
export class BubbleImageComponent implements OnInit {


  fileUploads: any[];

  constructor(private uploadService: FileUploadService) { }

  ngOnInit(): void {
    this.uploadService.getFiles(10000).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
    });
  }

  public getClassName(num: number) {
    while(num > 10){
    num = num - 10;
  }
  return num;
}
public getRandomNumber(min, max) { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min);
}
}
