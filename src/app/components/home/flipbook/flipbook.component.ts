import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flipbook',
  templateUrl: './flipbook.component.html',
  styleUrls: ['./flipbook.component.scss']
})
export class FlipbookComponent implements OnInit {

  showBook: boolean = true;
  showQr:boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  public toggleBook(){
    this.showBook = !this.showBook;
  }
}
