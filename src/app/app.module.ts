import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { BubbleImageComponent } from './components/home/bubble-image/bubble-image.component';
import { FlipbookComponent } from './components/home/flipbook/flipbook.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';

import { UploadFormComponent } from './components/uploader/upload-form/upload-form.component';
import { UploadListComponent } from './components/uploader/upload-list/upload-list.component';
import { UploadDetailsComponent } from './components/uploader/upload-details/upload-details.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    BubbleImageComponent,
    FlipbookComponent,
    UploadFormComponent,
    UploadListComponent,
    UploadDetailsComponent,
    UploaderComponent,
    HomeComponent,
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebase, "cloud")
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
